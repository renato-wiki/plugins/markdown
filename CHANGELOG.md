# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/) and this project adheres to
[Semantic Versioning](http://semver.org/).

## [Unreleased]

### Added
- Use markdown-it options from config
- Use markdown-it plugins from config

## [0.1.3] - 2017-05-28

### Added
- Support @renato-wiki/core@^0.3.0

## [0.1.2] - 2017-05-15

### Changed
- Update to renato-wiki@0.2.0

## [0.1.1] - 2017-05-12

### Changed
- Enable html and lang-prefix options

## [0.1.0] - 2017-04-25

### Added
- Create plain markdown parser plugin.
