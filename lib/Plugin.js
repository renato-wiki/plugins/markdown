/*!
 * Copyright (C) 2017 Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
 * Public License as published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */
"use strict";

const _ = require("lodash");
const fs = require("fs");
const Promise = require("bluebird");
const MarkdownIt = require("markdown-it");

const DEFAULT_CONFIG = require("./constants/default-config");

const fsReadFile = Promise.promisify(fs.readFile);

const META_REGEX = /^(\s*<!--json::RENATO-META::(.+)-->\s*\r?\n?)/;

/*===================================================== Exports  =====================================================*/

module.exports = Plugin;

/*==================================================== Functions  ====================================================*/

function Plugin(config) {
  this.CONTENT = new WeakMap();
  this.config = _.defaultsDeep({}, config, DEFAULT_CONFIG);
  this.md = new MarkdownIt(this.config);
  this.loadPlugins();
}

Plugin.prototype.loadPlugins = function () {
  _.each(this.config.plugins, (plugin) => {
    let name, parameters, module;
    if (typeof plugin === "string") {
      name = plugin;
      parameters = [];
    } else {
      name = plugin.module;
      parameters = plugin.parameters || [];
    }
    try {
      module = require(name);
    } catch(ignored) {
      module = require.main.require(name);
    }
    this.md.use(module, ...parameters);
  });
};

Plugin.prototype.getParsed = function (ignored, index) {
  if (!this.CONTENT.has(index)) { return Promise.reject({status: 404, error: new Error("Page not found.")}); }
  return this.CONTENT.get(index).parsed;
};

Plugin.prototype.getPlain = function (ignored, index) {
  if (!this.CONTENT.has(index)) { return Promise.reject({status: 404, error: new Error("Page not found.")}); }
  return this.CONTENT.get(index).plain;
};

Plugin.prototype.parse = function (string) { return this.md.render(string); };

Plugin.prototype.preparePageIndex = function (index, file) {
  return fsReadFile(file)
      .then((content) => {
        content = content.toString();
        let match = META_REGEX.exec(content);
        if (match != null) {
          content = content.substring(match[1].length);
          let meta;
          try {
            meta = JSON.parse(match[2]);
          } catch (e) {
            meta = {error: "Invalid meta data"};
          }
          _.assign(index.meta, meta);
        }
        this.CONTENT.set(index, {plain: content, parsed: this.parse(content)});
        return index;
      });
};
