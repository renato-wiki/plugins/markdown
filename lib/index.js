/*!
 * Copyright (C) 2017 Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
 * Public License as published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */
"use strict";

const Plugin = require("./Plugin");

const EXTENSIONS = [".md", ".markdown"];

/*===================================================== Exports  =====================================================*/

module.exports = use;

/*==================================================== Functions  ====================================================*/

function use(renato, config) {
  let plugin = new Plugin(config);
  for (let ext of EXTENSIONS) {
    renato.enableExtension(ext);
    renato.addHandler("core::index:update:build:" + ext, plugin.preparePageIndex.bind(plugin), -1);
    renato.addHandler("core::content:page:fetch:" + ext + ":parsed", plugin.getParsed.bind(plugin), -1);
    renato.addHandler("core::content:page:fetch:" + ext + ":plain", plugin.getPlain.bind(plugin), -1);
  }
  return plugin;
}
